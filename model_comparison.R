# model_comparison.R
# Ramon Perez Hernandez
# TFM - LANL
# R version: 3.4.0
# RStudio version: 1.0.143
# OS version: macOS Sierra - 10.12.5
# Last update: 02 Jul 2017

library(caret)
library(ranger)
library(e1071)
library(rpart)
library(klaR)
library(caTools)
library(kernlab)
library(tictoc)
library(dplyr)

# Once we have a suitable model which maximizes accuracy, we will ----
# compare the results obtained with the results obtained by other 
# algorithms in order to check if random forest has the best results, as
# the paper related to model comparison said. The algorithms we will use
# are decision trees, naive Bayes, logistic regression and SVM.

# We will start by importing the random forest optimal model: 
# model_7_ph2.
load("modeling/model_7_ph2.Rda")

# And we will import the dataset to obtain the data frame used for 
# building model_7_ph2.
load("preproc_files/lanl_prep_final.Rda")
lanl_model <- filter(lanl_prep_final, Main.Failure != "Undetermined") %>%
   mutate(Main.Failure = factor(Main.Failure), 
   mem_factor = factor(mem_factor)) %>% select (Different.Node, nodes,
   procstot_factor, mem, Down.Time, Time.Bet.Fail_Node, Year, 
   Year.Groups, Type.Day, Hour.Day, Time.Life, Active.Groups.Nodes, 
   Main.Failure)

# For model comparison, we will need the same conditions that we have 
# used in model_7_ph2: 10-fold cross validation with two repetitions.
control <- trainControl(method="repeatedcv", number=10, repeats=2)

   # -Decision trees (CART): we will use the rpart1SE method, which ----
   # is in rpart package. We have chosen this one because it does not 
   # have tuning parameters, so we can apply directly the cross 
   # validation.

set.seed(1111)
tic()
# Apply CART with 10-fold cross validation with two repetitions in 
# lanl_model. The rpart1SE method does not have tuning parameters and
# includes a model-specific variable importance metric. It is not
# necessary to scale the values, as in the random forest model.
# This may take a while.
model_rpart <- train(Main.Failure ~ ., lanl_model, method="rpart1SE", 
   metric="Kappa", trControl=control)
toc()

# Print the results. We have obtained:
# -Accuracy = 0.7520832 (SD = 0.006946823)
# -Kappa = 0.3576856 (SD = 0.01741308)
print("Results obtained")
print(model_rpart)
print(model_rpart$finalModel)
print(model_rpart$results)

# Here is the confusion matrix for this model. In this model, hardware
# and network failures are the only ones which have been well-allocated.
# In the case of software failures, it is confused with hardware
# failures. Facilities and Human.Error have a 0 value in their fields,
# so there are not being well-allocated.
confusionMatrix(model_rpart)

# Between the importance variables, we have Down.Time as the most
# important, followed by Year.Groups, Year, Active.Groups.Nodes or
# Different.Node. Time.Life and Time.Bet.Fail_Node, which have a 
# huge importance in random forest, are not so important here, because
# of the formulation of tree rules.
importance_rpart <- varImp(model_rpart)
print(importance_rpart)
plot(importance_rpart)

# In general, the results have been far worse than random forest
# case, with an accuracy of 0.75 and a Kappa of 0.35, approximately. It
# is very fast (30 seconds approximately), but it does not have a good
# performance.

# Save the model to avoid repeating the calculations.
if(!dir.exists("modeling")) {
   dir.create("modeling")
}
save(model_rpart, file="modeling/model_rpart.Rda")


   # -Naive Bayes: we will use the nb method, which is in klaR ----
   # in rpart package. It has three tuning parameters: fL (Laplace
   # Correction), usekernel (Distribution Type), adjust (Bandwidth 
   # Adjustment).

# We will use a small grid to test some values of the tuning parameters.
# In usekernel, it is necessary to fix it to TRUE. We will use the
# default value of fL (0), because we have seen there is not effect in
# the results if we change this value.
grid_nb <- expand.grid(fL=0, adjust=c(1,2,4,8), usekernel=TRUE)

set.seed(1111)
tic()
# Apply naive Bayes with 10-fold cross validation with two repetitions
# in lanl_model, using the tuneGrid defined before. The nb method 
# includes a model-specific variable importance metric. Again, it is 
# not necessary to scale the values.
# This may take a while.
model_nb <- train(Main.Failure ~ ., lanl_model, method="nb", 
   metric="Kappa", trControl=control, tuneGrid = grid_nb)
toc()

# Print the results. We have obtained:
# -Accuracy = 0.6981579 (SD = 0.0023161075)
# -Kappa = 0.017077231 (SD = 0.013509446)
# -fL = 0 / adjust = 1 / usekernel = TRUE 
print("Results obtained")
print(model_nb)
print(model_nb$results)
plot(model_nb)

# Here is the confusion matrix for this model. It is even worse than
# regression trees, because only hardware failures have been classified
# well, relatively. Software failures have a lot of failures, and 
# facilities, human error and network failures have all wrong values.
confusionMatrix(model_nb)

# In this case, importance is divided between Main.Failure values.
# In all cases, Down.Time and nodes values are the most important ones.
importance_nb <- varImp(model_nb)
print(importance_nb)
plot(importance_nb)

# The results are worse than decision trees, and spending more time to
# do the calculations (800 seconds approximately for all cases in the
# grid).

# Save the model to avoid repeating the calculations.
if(!dir.exists("modeling")) {
   dir.create("modeling")
}
save(model_nb, file="modeling/model_nb.Rda")


   # -Logistic regression: we will use the LogitBoost method, which ----
   # is in caTools package. It has only one tuning parameter, which is
   # nIter (number of Boosting Iterations), whose default value is the
   # number of variables of the data frame.

# We will use a grid to test some nIter values, from 1 to 20 (remember
# we have 13 variables in lanl_model).
grid_lr <- expand.grid(nIter=1:20)

set.seed(1111)
tic()
# Apply logistic regression with 10-fold cross validation with two 
# repetitions in lanl_model, using the tuneGrid defined before. The 
# LogitBoost method includes a model-specific variable importance metric.
# Again, it is not necessary to scale the values.
# This may take a while.
model_lr <- train(Main.Failure ~ ., lanl_model, method="LogitBoost", 
   metric="Kappa", trControl=control, tuneGrid = grid_lr)
toc()

# Print the results. We have obtained:
# -Accuracy = 0.7921911 (SD = 0.006597301)
# -Kappa = 0.2904479 (SD = 0.02432087)
# -nIter = 9
print("Results obtained")
print(model_lr)
print(model_lr$finalModel)
print(model_lr$results)
plot(model_lr)

# Here is the confusion matrix for this model. We have better results
# than in the other cases, but again we have too much failures in
# facilities, human error and network failures (we have not succeeded in
# any case in the last two).
confusionMatrix(model_lr)

# The importance variables are similar to naive Bayes case, because we
# have the results divided by Main.Failure values, and again Down.Time
# and nodes presents the best results.
importance_lr <- varImp(model_lr)
print(importance_lr)
plot(importance_lr)

# The results have improved in this case (Accuracy greater than 0.79),
# but they are still far from random forest, as we have a very low 
# Kappa value, and only hardware failures are well-allocated.

# Save the model to avoid repeating the calculations.
if(!dir.exists("modeling")) {
   dir.create("modeling")
}
save(model_lr, file="modeling/model_lr.Rda")


   # -SVM: we will use the svmLinear method, which is in kernlab ----
   # package. It has one tuning parameter, which is C (Cost). It uses
   # a linear kernel to build the model. We have chosen this one for
   # being the easiest SVM model in terms on kernel definition and
   # number of tuning parameters

# We will only use C = 0.1 for building the model, because it takes too
# much time to make the calculations if we choose a grid with several
# values (several hours, or even more). We have decided a value of 0.1 
# because we did some tests with a lower K-fold (K=2), doing a random 
# search of the optimal C value. The result was C=0.073 approximately, 
# so we have chosen C=0.1 for being next to 0.073. Moreover, the other 
# results (1 and 10, approximately) have worse results and without 
# changes between them.
grid_svm <- expand.grid(C=0.1)

set.seed(1111)
tic()
# Apply SVM with 10-fold cross validation with two repetitions in 
# lanl_model, using C=0.1. The svmLinear method includes a 
# model-specific variable importance metric. In this case, we will
# scale the values, as SVM relies on distance metrics to find the 
# hyperplanes which separate the different clases. This may take a while.
model_svm <- train(Main.Failure ~ ., lanl_model, method="svmLinear", 
   metric="Kappa", trControl=control, tuneGrid=grid_svm,
   preProcess=c("center", "scale"))
toc()

# Print the results. We have obtained:
# -Accuracy = 0.727033 (SD = 0.006012092)
# -Kappa = 0.2876019 (SD = 0.01582313)
# -C = 0.1
print("Results obtained")
print(model_svm)
print(model_svm$finalModel)
print(model_svm$results)

# Here is the confusion matrix for this model. The results are similar
# to the other cases: the only hardware type which predicts well is
# hardware, software presents more errors than hits, and the rest of
# cases are all wrong.
confusionMatrix(model_svm)

# The importance variables are similar to naive Bayes and Logistic
# Regression cases (results divided by Main.Failure values and Down.Time
# and nodes presents the best results).
importance_svm <- varImp(model_svm)
print(importance_svm)
plot(importance_svm)

# This model do not improve at all the results obtained with the other
# models, as we have seen before.

# Save the model to avoid repeating the calculations.
if(!dir.exists("modeling")) {
   dir.create("modeling")
}
save(model_svm, file="modeling/model_svm.Rda")


# And what model is the best? For that purpose, we will use again ----
# function resamples, from caret.
resamps <- resamples(list(rf = model_7_ph2, rpart = model_rpart,
   nb = model_nb, lr = model_lr, svm = model_svm))
resamps
summary(resamps)

# It is clear that random forest is the best model by far. It is also
# true that the dataset have been designed to optimize this model, so
# the other models may have obtained better results with other election
# of variables, but it is not the purpose of this study.
# We can see the results better with graphics:

bwplot(resamps, layout = c(3, 1))
dotplot(resamps, metric = "Kappa")
dotplot(resamps, metric = "Accuracy")

# In the other models, in terms of Accuracy, we can see logistic
# regression has the second best result, reaching a value of 0.80 as a
# maximum, followed by decision trees, SVM and naive Bayes.
# However, according to Kappa value, decision tree is the second best
# one, with logistic regression in the third position. Again, naive
# Bayes has the worst results, as the reference paper of the models
# comparative said.
# In terms of computing time, remember that decision trees has a good
# computing time, and it is the fastest model to build. At the other
# end, we have SVM, which is the slowest by far. The others have a 
# normal performance.

# So, summing up, random forest is the best model for this dataset, and
# naive Bayes is the worst, which matches the conclusions of the
# model comparative paper. In this paper, decision trees had better
# results than logistic regression and SVM, staying at the second place,
# which is true in this study if we consider Kappa metric (parameter to
# be optimized in all models).
