## ANALYSIS AND DESIGN OF PREDICTOR OF FAILURES IN SUPERCOMPUTERS THROUGH MACHINE LEARNING TECHNIQUES ##
### [Ramón Pérez Hernández](https://www.linkedin.com/in/ramón-pérez-hernández-4bb355a9)
** Master in Telecommunication Engineering - Universidad Politécnica de Madrid**

End of Master Project

### Quick summary ###

This repository contains the code and files generated and used for the End of Master Project whose title is "Analysis and Design of Predictor of Failures in Supercomputers through Machine Learning Techniques", by Ramón Pérez Hernández.

* R version: 3.4.0
* RStudio version: 1.0.143
* Shiny version: 1.0.3

### What can I find here? ###

Repository structure is as follows:

* Eleven R scripts with all the steps followed in the project. The execution order is:

   1. download_dataset.R
   2. quick_exploration.R
   3. general_preprocessing.R
   4. exploration_by_system.R
   5. preprocessing_by_system.R
   6. creating_features.R
   7. first_visualization_analysis.R
   8. last_visualization_analysis.R
   9. model_design.R
   10. model_comparison.R
   11. model_results.R (includes a Shiny app)

* Directory knitr_html: HTML documentation of every R script, generated by knitr.
* raw_files: contains the raw dataset.
* preproc_files: data frames generated in preprocessing phase.
* modeling: models and data frames generated in modeling phase.

### GPL License ###

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
