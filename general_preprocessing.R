# general_preprocessing.R
# Ramon Perez Hernandez
# TFM - LANL
# R version: 3.4.0
# RStudio version: 1.0.143
# OS version: macOS Sierra - 10.12.5
# Last update: 01 Jul 2017

# We will do some general pre-processing in the raw dataset after doing 
# the quick exploration.
lanl_raw <- read.csv('raw_files/lanl.csv', header = TRUE, sep = ",", 
   skip = 1)

# We can see that Same.Event attribute is always 'No', so we can delete 
# it from the data frame. We will save the changes into another data 
# frame (lanl_prep) to keep the original one, just in case.
library(dplyr)
lanl_prep <- select(lanl_raw, (-Same.Event))

# In quick_exploration.R, we have seen there are 126 empty values in 
# machine.type, fru.type and purpose columns, and there are 126 NA's in
# numeric columns like procstot, procsinnode or nodenum too (among 
# others).
# Let's see if there is a specific system which have these empty values.
table(filter(lanl_prep, machine.type == "")$System)

# For machine.type, system 17 has all the empty values. And for some 
# examples of numeric colums?
table(filter(lanl_prep, is.na(procstot))$System)
table(filter(lanl_prep, is.na(nodenum))$System)

# Same issue, so system 17 seems to have all the empty values mentioned 
# before.
# But how many rows does it have? 126 rows, too.
table(lanl_prep$System)

# If we take a look of some rows of system 17, we will see all columns 
# have NA or empty values, excepting Prob.Started/Fixed, Down.Time and 
# Failure-type columns.
head(filter(lanl_prep, System == 17))

# And remember we do not have any information about system 17 in the 
# official documentation. For all those reason, we will remove all the 
# rows of system 17. 
lanl_prep <- filter(lanl_prep, System != 17)

# Now, there are not NA values in the dataset.
table(is.na(lanl_prep))

# We can see machine.type, node.install/prod/decom, fru.type and purpose
# columns still maintain the empty value as a level. Using as.character 
# and as.factor, we will delete it.
lanl_prep <- mutate(lanl_prep, 
   machine.type = as.factor(as.character(lanl_prep$machine.type)),
   node.install = as.factor(as.character(lanl_prep$node.install)),
   node.prod = as.factor(as.character(lanl_prep$node.prod)),
   node.decom = as.factor(as.character(lanl_prep$node.decom)),
   fru.type = as.factor(as.character(lanl_prep$fru.type)),
   purpose = as.factor(as.character(lanl_prep$purpose)))

# About failure's classification, we can see that every kind of failure 
# is described in one column. Moreover, excepting Human.Error and Network
# attributes, all of them have subcategories (for example, hardware
# failures can be related to CPU, Memory Dimm...).
summary(select(lanl_prep, (Facilities:Software)))

# For that reason, we are going to transform these columns to clarify the
# main reason of the failure. We will not consider failure subcategory in
# this study. The new column will be called Main.Failure.
lanl_prep["Main.Failure"] <- NA

# Convert failure's columns to new format.
failures = c("Facilities", "Hardware", "Human.Error", "Network", 
   "Undetermined", "Software")
for (f in failures) {
   pos <- which(lanl_prep[f] != '')
   lanl_prep$Main.Failure[pos] <- f
}

# We can see there are still empty values (453).
table(is.na(lanl_prep$Main.Failure))

# We will treat them as 'Undetermined'.
pos <- which(is.na(lanl_prep$Main.Failure))
lanl_prep$Main.Failure[pos] <- 'Undetermined'

# After that, every row has a value in the new column.
table(is.na(lanl_prep$Main.Failure))

# Finally, convert column to factor.
lanl_prep$Main.Failure <- as.factor(lanl_prep$Main.Failure)
summary(select(lanl_prep, Main.Failure))

# We can delete columns related to individual kind of failures because 
# they are useless now.
lanl_prep <- select(lanl_prep, -(Facilities:Software))

# Now, we will transform Prob.Started..mm.dd.yy.hh.mm. and 
# Prob.Fixed..mm.dd.yy.hh.mm. columns from factor to date. About 
# node.install, node.prod and node.decom columns, we will transform them
# conveniently after studying each system separately.
lanl_prep <- mutate(lanl_prep, Prob.Started = 
   as.POSIXct(as.character(lanl_prep$Prob.Started..mm.dd.yy.hh.mm.), 
   format = "%m/%d/%Y %H:%M", tz = "GMT"), Prob.Fixed = 
   as.POSIXct(as.character(lanl_prep$Prob.Fixed..mm.dd.yy.hh.mm.), 
   format = "%m/%d/%Y %H:%M", tz = "GMT"))

# We can see there are not NA values in the new columns. If we had not 
# specified GMT time zone, we would have obtained some NA values due
# to the daylight saving time.
table(is.na(lanl_prep$Prob.Started))
table(is.na(lanl_prep$Prob.Fixed))

# Obviously, Prob.Started is prior to Prob.Fixed (we use <= because there
# are some nodes where Prob.Started = Prob.Fixed).
table(lanl_prep$Prob.Started <= lanl_prep$Prob.Fixed)

# We can delete old Prob.Started and Prob.Fixed columns after these
# transformations. We will also arrange the columns to maintain the 
# original order.
lanl_prep <- select(lanl_prep, -Prob.Started..mm.dd.yy.hh.mm., 
   -Prob.Fixed..mm.dd.yy.hh.mm.) %>% select(System:purpose, 
   Prob.Started:Prob.Fixed, Down.Time:Main.Failure)

# After that, we will check if Down.Time values are correct, comparing 
# them with the difference between Prob.Fixed and Prob.Started.
lanl_prep <- mutate(lanl_prep, Calc.Down.Time = 
   as.numeric(lanl_prep$Prob.Fixed - lanl_prep$Prob.Started, 
   units = "mins"))
table(lanl_prep$Down.Time == lanl_prep$Calc.Down.Time)

# There are 423 values which are not equal. Let's analyze them.
errors_position <- which(lanl_prep$Down.Time != lanl_prep$Calc.Down.Time)
head(select(lanl_prep, Prob.Started, Prob.Fixed, Down.Time, 
   Calc.Down.Time)[errors_position,])

# If we calculate the difference between the down times, we will see that
# all the values obtained are 1 minute or 1 hour, so we can assume dates
# are correct and the original down times are wrong. 
abs(lanl_prep$Down.Time - lanl_prep$Calc.Down.Time)[errors_position]

# We will delete Down.Time column, reorder all the columns and rename
# Calc.Down.Time.
lanl_prep <- select(lanl_prep, -Down.Time) %>% select(System:Prob.Fixed,
   Calc.Down.Time, Main.Failure) %>% rename(Down.Time = Calc.Down.Time)

# Summary and structure of transformed dataset.
summary(lanl_prep)
str(lanl_prep)

# Finally, we will save the preprocessed dataset in Rda format into a new
# directory called prepoc_files.
if(!dir.exists("preproc_files")) {
   dir.create("preproc_files")
}

save(lanl_prep, file="preproc_files/lanl_prep.Rda")
